package cc.dewdrop.v2ex.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import cc.dewdrop.v2ex.R;
import cc.dewdrop.v2ex.adapter.TopicAdapter;
import cc.dewdrop.v2ex.tasks.GetLatestTopicsTask;
import cc.dewdrop.v2ex.tasks.GetTopicRepliesTask;
import cc.dewdrop.v2ex.ui.TopicActivity;
import cc.dewdrop.v2ex.utils.AppMacros;

/**
 * A simple {@link Fragment} subclass.
 */
public class LatestTopicFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, TopicAdapter.TopicItemListener {

    private static LatestTopicFragment latestTopicFragment;

    private SwipeRefreshLayout refreshLayout;

    private RecyclerView recyclerView;
    private TopicAdapter topicAdapter;

    private View rootView;


    public LatestTopicFragment() {
        // Required empty public constructor
    }

    public static LatestTopicFragment getInstance() {
        if (latestTopicFragment == null) {
            synchronized (LatestTopicFragment.class) {
                if (latestTopicFragment == null) {
                    latestTopicFragment = new LatestTopicFragment();
                }
            }
        }
        return latestTopicFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_latest_topic, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        topicAdapter = new TopicAdapter(getActivity(), AppMacros.latestTopics);
        topicAdapter.setTopicItemListener(this);
        recyclerView.setAdapter(topicAdapter);

        refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refreshLayout);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setRefreshing(true);

        GetLatestTopicsTask latestTopicsTask = new GetLatestTopicsTask(getActivity());
        latestTopicsTask.setTopicAdapter(topicAdapter);
        latestTopicsTask.setRefreshLayout(refreshLayout);
        latestTopicsTask.execute();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onRefresh() {
        if (!refreshLayout.isRefreshing()) {
            GetLatestTopicsTask latestTopicsTask = new GetLatestTopicsTask(getActivity());
            latestTopicsTask.setTopicAdapter(topicAdapter);
            latestTopicsTask.setRefreshLayout(refreshLayout);
            latestTopicsTask.execute();
        }
    }

    @Override
    public void onItemClick(int position) {
        showTopic(position);
    }
    private void showTopic(int position) {
        GetTopicRepliesTask getTopicRepliesTask = new GetTopicRepliesTask(getActivity());
        getTopicRepliesTask.setTopicId(AppMacros.latestTopics.get(position).getId());
        getTopicRepliesTask.setPosition(position);
        getTopicRepliesTask.execute("latest");
    }
}
