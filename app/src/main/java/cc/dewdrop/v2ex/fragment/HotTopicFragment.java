package cc.dewdrop.v2ex.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import cc.dewdrop.v2ex.R;
import cc.dewdrop.v2ex.adapter.TopicAdapter;
import cc.dewdrop.v2ex.bean.Topic;
import cc.dewdrop.v2ex.tasks.GetHotTopicsTask;
import cc.dewdrop.v2ex.tasks.GetLatestTopicsTask;
import cc.dewdrop.v2ex.tasks.GetTopicRepliesTask;
import cc.dewdrop.v2ex.ui.TopicActivity;
import cc.dewdrop.v2ex.utils.AppMacros;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotTopicFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, TopicAdapter.TopicItemListener {

    private static HotTopicFragment hotTopicFragment;

    private SwipeRefreshLayout refreshLayout;

    private RecyclerView recyclerView;
    private TopicAdapter topicAdapter;

    private View rootView;

    public HotTopicFragment() {
        // Required empty public constructor
    }

    public static HotTopicFragment getInstance() {
        if (hotTopicFragment == null) {
            synchronized (HotTopicFragment.class) {
                if (hotTopicFragment == null) {
                    hotTopicFragment = new HotTopicFragment();
                }
            }
        }
        return hotTopicFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_hot_topic, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        topicAdapter = new TopicAdapter(getActivity(), AppMacros.hotTopics);
        topicAdapter.setTopicItemListener(this);
        recyclerView.setAdapter(topicAdapter);

        refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refreshLayout);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setRefreshing(true);

        GetHotTopicsTask hotTopicsTask = new GetHotTopicsTask(getActivity());
        hotTopicsTask.setTopicAdapter(topicAdapter);
        hotTopicsTask.setRefreshLayout(refreshLayout);
        hotTopicsTask.execute();
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onItemClick(int position) {
        showTopic(position);
    }

    private void showTopic(int position) {
        GetTopicRepliesTask getTopicRepliesTask = new GetTopicRepliesTask(getActivity());
        getTopicRepliesTask.setTopicId(AppMacros.hotTopics.get(position).getId());
        getTopicRepliesTask.setPosition(position);
        getTopicRepliesTask.execute("hot");
    }
}
