package cc.dewdrop.v2ex.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cc.dewdrop.v2ex.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NodesFragment extends Fragment {

    private static NodesFragment nodesFragment;


    public NodesFragment() {
        // Required empty public constructor
    }

    public static NodesFragment getInstance() {
        if (nodesFragment == null) {
            synchronized (NodesFragment.class) {
                if (nodesFragment == null) {
                    nodesFragment = new NodesFragment();
                }
            }
        }
        return nodesFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nodes, container, false);
    }


}
