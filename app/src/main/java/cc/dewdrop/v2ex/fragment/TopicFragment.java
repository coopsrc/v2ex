package cc.dewdrop.v2ex.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cc.dewdrop.v2ex.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopicFragment extends Fragment {
    private static TopicFragment topicFragment;




    public TopicFragment() {

    }

    public static TopicFragment getInstance() {
        if (topicFragment == null) {
            synchronized (TopicFragment.class) {
                if (topicFragment == null) {
                    topicFragment = new TopicFragment();
                }
            }
        }
        return topicFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_topics, container, false);
    }


}
