package cc.dewdrop.v2ex.tasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import cc.dewdrop.v2ex.ui.TopicActivity;
import cc.dewdrop.v2ex.utils.APIUtils;
import cc.dewdrop.v2ex.utils.AppMacros;
import cc.dewdrop.v2ex.utils.JsonUtils;

/**
 * Created by Tingkuo on 2015/8/30.
 */
public class GetTopicRepliesTask extends AsyncTask<String, Integer, String> {
    private Context context;

    private Long topicId;

    private int position;

    private String type = "";

    public GetTopicRepliesTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s != null && !s.equals("")) {

            AppMacros.replies.put(topicId, JsonUtils.getReplies(s));

            Intent intent = new Intent(context, TopicActivity.class);
            intent.putExtra("position", position);
            intent.putExtra("topic_id", topicId);
            if (type.equals("latest")) {
                intent.putExtra("type", type);
            } else if (type.equals("hot")) {
                intent.putExtra("type", type);
            }
            context.startActivity(intent);

        } else {
            Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(String s) {
        super.onCancelled(s);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected String doInBackground(String... params) {
        type = params[0];
        return APIUtils.getTopicReplies("" + topicId);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }
}
