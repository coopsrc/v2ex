package cc.dewdrop.v2ex.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;

import cc.dewdrop.v2ex.adapter.TopicAdapter;
import cc.dewdrop.v2ex.utils.APIUtils;
import cc.dewdrop.v2ex.utils.AppMacros;
import cc.dewdrop.v2ex.utils.JsonUtils;

/**
 * Created by Arlen on 2015/8/26.
 */
public class GetLatestTopicsTask extends AsyncTask<Void, Integer, String> {
    private Context context;

    private TopicAdapter topicAdapter;
    private SwipeRefreshLayout refreshLayout;

    public GetLatestTopicsTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
//        refreshLayout.setRefreshing(false);
        if (s != null && !s.equals("")) {
            try {
                AppMacros.latestTopics = JsonUtils.getTopics(s);
                topicAdapter.setTopics(AppMacros.latestTopics);
                topicAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(String s) {
        super.onCancelled(s);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected String doInBackground(Void... params) {
        return APIUtils.getLatestTopics();
    }

    public void setTopicAdapter(TopicAdapter topicAdapter) {
        this.topicAdapter = topicAdapter;
    }

    public void setRefreshLayout(SwipeRefreshLayout refreshLayout) {
        this.refreshLayout = refreshLayout;
    }
}
