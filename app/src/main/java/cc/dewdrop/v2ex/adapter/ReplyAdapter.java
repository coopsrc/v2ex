package cc.dewdrop.v2ex.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import cc.dewdrop.v2ex.R;
import cc.dewdrop.v2ex.bean.Reply;

/**
 * Created by Tingkuo on 2015/8/30.
 */
public class ReplyAdapter extends RecyclerView.Adapter<ReplyAdapter.ViewHolder> {

    private Context context;
    private List<Reply> replyList;

    public ReplyAdapter(Context context, List<Reply> replyList) {
        this.context = context;
        this.replyList = replyList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.layout_item_reply, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Reply reply = replyList.get(position);
        Glide.with(context).load(reply.getMember().getAvatarLarge()).into(holder.imageViewMember);
        holder.textViewUserName.setText(reply.getMember().getUserName());
        holder.textViewTime.setText("" + reply.getCreated());
        holder.textViewIndex.setText("" + position);
        holder.textViewContent.setText(reply.getContent());
    }

    @Override
    public int getItemCount() {
        return replyList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        private ImageView imageViewMember;
        private TextView textViewUserName;
        private TextView textViewTime;
        private TextView textViewIndex;
        private TextView textViewContent;

        public ViewHolder(View itemView) {
            super(itemView);
            imageViewMember = (ImageView) itemView.findViewById(R.id.imageViewMember);
            textViewUserName = (TextView) itemView.findViewById(R.id.textViewUserName);
            textViewTime = (TextView) itemView.findViewById(R.id.textViewTime);
            textViewIndex = (TextView) itemView.findViewById(R.id.textViewIndex);
            textViewContent = (TextView) itemView.findViewById(R.id.textViewContent);
        }
    }
}
