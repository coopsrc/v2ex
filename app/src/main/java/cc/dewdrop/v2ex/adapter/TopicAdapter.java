package cc.dewdrop.v2ex.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import cc.dewdrop.v2ex.R;
import cc.dewdrop.v2ex.bean.Topic;
import cc.dewdrop.v2ex.ui.TopicActivity;
import cc.dewdrop.v2ex.utils.AppMacros;

/**
 * Created by Arlen on 2015/8/26.
 */
public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.ViewHolder> {

    private Context context;
    private List<Topic> topics = new ArrayList<>();

    private TopicItemListener topicItemListener;

    public interface TopicItemListener {
        void onItemClick(int position);
    }


    public TopicAdapter(Context context, List<Topic> topics) {
        this.context = context;
        this.topics = topics;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.layout_item_topic, null);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Topic topic = topics.get(position);

        holder.position = position;

        Glide.with(context).load(topic.getMember().getAvatarLarge()).into(holder.imageViewMember);

        holder.textViewTitle.setText(topic.getTitle());

        Glide.with(context).load(topic.getNode().getAvatarLarge()).into(holder.imageViewNode);
        holder.textViewNode.setText(topic.getNode().getTitle());

        holder.textViewContent.setText(topic.getContent());

        holder.textViewMember.setText(topic.getMember().getUserName());
        holder.textViewTime.setText("" + topic.getCreated());

    }

    @Override
    public int getItemCount() {
        return topics.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private View rootView;

        private ImageView imageViewMember;
        private TextView textViewTitle;
        private ImageView imageViewNode;
        private TextView textViewNode;
        private TextView textViewContent;
        private TextView textViewMember;
        private TextView textViewTime;

        private int position;

        public ViewHolder(View itemView) {
            super(itemView);
            imageViewMember = (ImageView) itemView.findViewById(R.id.imageViewMember);

            textViewTitle = (TextView) itemView.findViewById(R.id.textViewTitle);

            imageViewNode = (ImageView) itemView.findViewById(R.id.imageViewNode);
            textViewNode = (TextView) itemView.findViewById(R.id.textViewNode);

            textViewContent = (TextView) itemView.findViewById(R.id.textViewContent);

            textViewMember = (TextView) itemView.findViewById(R.id.textViewMember);
            textViewTime = (TextView) itemView.findViewById(R.id.textViewTime);

            rootView = itemView.findViewById(R.id.cardView);

            rootView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (topicItemListener != null){
                topicItemListener.onItemClick(position);
            }
        }
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }

    public TopicItemListener getTopicItemListener() {
        return topicItemListener;
    }

    public void setTopicItemListener(TopicItemListener topicItemListener) {
        this.topicItemListener = topicItemListener;
    }
}
