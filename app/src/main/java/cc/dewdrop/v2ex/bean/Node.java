package cc.dewdrop.v2ex.bean;

/**
 * Created by Arlen on 2015/8/26.
 */
public class Node {
    private long id;
    private String name;
    private String title;
    private String titleAlterNative;
    private String url;
    private long topics;
    private String avatarMini;
    private String avatarNormal;
    private String avatarLarge;
    private long created;
    private long lastModified;
    private long lastTouched;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleAlterNative() {
        return titleAlterNative;
    }

    public void setTitleAlterNative(String titleAlterNative) {
        this.titleAlterNative = titleAlterNative;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getTopics() {
        return topics;
    }

    public void setTopics(long topics) {
        this.topics = topics;
    }

    public String getAvatarMini() {
        return avatarMini;
    }

    public void setAvatarMini(String avatarMini) {
        this.avatarMini = avatarMini;
    }

    public String getAvatarNormal() {
        return avatarNormal;
    }

    public void setAvatarNormal(String avatarNormal) {
        this.avatarNormal = avatarNormal;
    }

    public String getAvatarLarge() {
        return avatarLarge;
    }

    public void setAvatarLarge(String avatarLarge) {
        this.avatarLarge = avatarLarge;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public long getLastTouched() {
        return lastTouched;
    }

    public void setLastTouched(long lastTouched) {
        this.lastTouched = lastTouched;
    }
}
