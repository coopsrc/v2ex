package cc.dewdrop.v2ex.ui;

import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import cc.dewdrop.v2ex.R;
import cc.dewdrop.v2ex.adapter.ReplyAdapter;
import cc.dewdrop.v2ex.bean.Reply;
import cc.dewdrop.v2ex.utils.AppMacros;

public class TopicActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;

    private ImageView imageViewAvatar;

    private Intent intent;

    private int position;
    private String type;
    private String avatar;
    private String title;
//    private String content;

//    private TextView textViewContent;

    private RecyclerView recyclerView;
    private ReplyAdapter replyAdapter;
    private List<Reply> replyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);


        intent = getIntent();
        position = intent.getIntExtra("position", 0);
        type = intent.getStringExtra("type");
        if (type.equals("latest")) {
            avatar = AppMacros.latestTopics.get(position).getMember().getAvatarLarge();
            title = AppMacros.latestTopics.get(position).getMember().getUserName();
//            content = AppMacros.latestTopics.get(position).getContent();
            replyList = AppMacros.replies.get(AppMacros.latestTopics.get(position).getId());
        } else if (type.equals("hot")) {
            avatar = AppMacros.hotTopics.get(position).getMember().getAvatarLarge();
            title = AppMacros.hotTopics.get(position).getMember().getUserName();
//            content = AppMacros.hotTopics.get(position).getContent();
            replyList = AppMacros.replies.get(AppMacros.hotTopics.get(position).getId());
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
        collapsingToolbarLayout.setTitle(title);

        imageViewAvatar = (ImageView) findViewById(R.id.imageViewAvatar);

        Glide.with(this).load(avatar).centerCrop().into(imageViewAvatar);

//        textViewContent = (TextView) findViewById(R.id.textViewContent);
//        textViewContent.setText(content);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        replyAdapter = new ReplyAdapter(this, replyList);
        recyclerView.setAdapter(replyAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_topic, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_settings:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
