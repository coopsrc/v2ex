package cc.dewdrop.v2ex.utils;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by Arlen on 2015/8/26.
 */
public class HttpUtils {
    private static OkHttpClient httpClient = new OkHttpClient();

    public static String syncGetString(String url) throws Exception {
        Request request = new Request.Builder().url(url).build();
        Response response = httpClient.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code" + response);
        }

        String result = response.body().string();
        return result;
    }

    public static String asyncGetString(String url) throws Exception {
        final String[] result = {""};

        Request request = new Request.Builder().url(url).build();
        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code" + response);
                }
                Headers headers = response.headers();
                for (int i = 0; i < headers.size(); i++) {
                    System.out.println(headers.name(i) + ": " + headers.value(i));
                }
                System.out.println(response.body().string());
                result[0] = response.body().string();
            }
        });
        return result[0];
    }

    public static void syncPostString(String url, String content, MediaType mediaType) throws Exception {
        Request request = new Request.Builder().url(url).post(RequestBody.create(mediaType, content)).build();
        Response response = httpClient.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }
        System.out.println(response.body().string());
    }

    public static void asyncPostString(String url, String content, MediaType mediaType) throws Exception {
        Request request = new Request.Builder().url(url).post(RequestBody.create(mediaType, content)).build();
        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                }
                System.out.println(response.body().string());
            }
        });
    }
}
