package cc.dewdrop.v2ex.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cc.dewdrop.v2ex.bean.Member;
import cc.dewdrop.v2ex.bean.Node;
import cc.dewdrop.v2ex.bean.Reply;
import cc.dewdrop.v2ex.bean.Topic;

/**
 * Created by Arlen on 2015/8/26.
 */
public class JsonUtils {
    public static List<Topic> getTopics(String content) {
        List<Topic> topics = new ArrayList<>();

        try {
            JSONArray topicArray = new JSONArray(content);
            for (int i = 0; i < topicArray.length(); ++i) {
                JSONObject topicObject = topicArray.optJSONObject(i);

                Topic topic = new Topic();
                topic.setId(topicObject.getLong("id"));
                topic.setTitle(topicObject.getString("title"));
                topic.setUrl(topicObject.getString("url"));
                topic.setContent(topicObject.getString("content"));
                topic.setContentRendered(topicObject.getString("content_rendered"));
                topic.setReplies(topicObject.getLong("replies"));

                JSONObject memberObject = topicObject.getJSONObject("member");
                Member member = new Member();
                member.setId(memberObject.getLong("id"));
                member.setUserName(memberObject.getString("username"));
                member.setTagLine(memberObject.getString("tagline"));
                member.setAvatarMini(AppMacros.HTTP + memberObject.getString("avatar_mini"));
                member.setAvatarNormal(AppMacros.HTTP + memberObject.getString("avatar_normal"));
                member.setAvatarLarge(AppMacros.HTTP + memberObject.getString("avatar_large"));
                topic.setMember(member);

                JSONObject nodeObject = topicObject.getJSONObject("node");
                Node node = new Node();
                node.setId(nodeObject.getLong("id"));
                node.setName(nodeObject.getString("name"));
                node.setTitle(nodeObject.getString("title"));
                node.setTitleAlterNative(nodeObject.getString("title_alternative"));
                node.setUrl(nodeObject.getString("url"));
                node.setTopics(nodeObject.getLong("topics"));
                node.setAvatarMini(AppMacros.HTTP + nodeObject.getString("avatar_mini"));
                node.setAvatarNormal(AppMacros.HTTP + nodeObject.getString("avatar_normal"));
                node.setAvatarLarge(AppMacros.HTTP + nodeObject.getString("avatar_large"));
                topic.setNode(node);

                topic.setCreated(topicObject.getLong("created"));
                topic.setLastModified(topicObject.getLong("last_modified"));
                topic.setLastTouched(topicObject.getLong("last_touched"));

                topics.add(topic);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return topics;
    }

    public static List<Reply> getReplies(String content) {
        List<Reply> replyList = new ArrayList<>();

        try {
            JSONArray replyArray = new JSONArray(content);
            for (int i = 0; i < replyArray.length(); ++i) {
                JSONObject replyObject = replyArray.optJSONObject(i);
                Reply reply = new Reply();
                reply.setId(replyObject.getLong("id"));
                reply.setThanks(replyObject.getString("thanks"));
                reply.setContent(replyObject.getString("content"));
                reply.setContentRendered(replyObject.getString("content_rendered"));

                JSONObject memberObject = replyObject.getJSONObject("member");
                Member member = new Member();
                member.setId(memberObject.getLong("id"));
                member.setUserName(memberObject.getString("username"));
                member.setTagLine(memberObject.getString("tagline"));
                member.setAvatarMini(AppMacros.HTTP + memberObject.getString("avatar_mini"));
                member.setAvatarNormal(AppMacros.HTTP + memberObject.getString("avatar_normal"));
                member.setAvatarLarge(AppMacros.HTTP + memberObject.getString("avatar_large"));
                reply.setMember(member);

                reply.setCreated(replyObject.getLong("created"));
                reply.setLastModified(replyObject.getLong("last_modified"));
                replyList.add(reply);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return replyList;
    }
}
