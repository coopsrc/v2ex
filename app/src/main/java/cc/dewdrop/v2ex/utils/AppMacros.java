package cc.dewdrop.v2ex.utils;

import com.squareup.okhttp.MediaType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cc.dewdrop.v2ex.bean.Reply;
import cc.dewdrop.v2ex.bean.Topic;

/**
 * Created by Arlen on 2015/8/26.
 */
public class AppMacros {
    //api
    public static final String HTTP = "http:";
    //域名
    private static final String DOMAIN_NAME = "http://www.v2ex.com";
    //获取社区信息统计
    public static final String SITE_STATUS = DOMAIN_NAME + "/api/site/stats.json";
    //获取社区介绍
    public static final String SITE_INFO = DOMAIN_NAME + "/api/site/info.json";
    //获取全部节点
    //获取耽搁节点信息
    //获取最新主题
    public static final String LATEST_TOPIC = DOMAIN_NAME + "/api/topics/latest.json";
    //获取热门主题
    public static final String HOT_TOPIC = DOMAIN_NAME + "/api/topics/hot.json";
    //获取主题回复
    public static final String TOPIC_REPLY = DOMAIN_NAME + "/api/replies/show.json?topic_id=";


    //MediaType
    public static final MediaType MEDIA_TYPE_MARKDOWN = MediaType.parse("text/x-markdown; charset=utf-8");


    //
    public static List<Topic> latestTopics = new ArrayList<>();
    public static List<Topic> hotTopics = new ArrayList<>();
    public static Map<Long, List<Reply>> replies = new HashMap<>();

}
