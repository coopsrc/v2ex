package cc.dewdrop.v2ex.utils;

/**
 * Created by Arlen on 2015/8/26.
 */
public class APIUtils {
    public static String getLatestTopics() {
        try {
            return HttpUtils.syncGetString(AppMacros.LATEST_TOPIC);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getHotTopics() {
        try {
            return HttpUtils.syncGetString(AppMacros.HOT_TOPIC);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTopicReplies(String topicId) {
        try {
            return HttpUtils.syncGetString(AppMacros.TOPIC_REPLY + topicId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
